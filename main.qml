import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5

Window {
    width: 400
    height: 300
    visible: true
    title: qsTr("Hello World")

    Column {
        id: col
        spacing: 12
        anchors {
            centerIn: parent
        }

        Text {
            id: atext
            text: qsTr(fooCtrl.theText)
            anchors {
                horizontalCenter: abutton.horizontalCenter
            }
        }

        Button {
            id: abutton
            text: qsTr("click me")
            onClicked: {
                console.debug("Button clicked")
				fooCtrl.changeTheText()
            }
			anchors {
				horizontalCenter: col.horizontalCenter
			}
        }
    }

}
