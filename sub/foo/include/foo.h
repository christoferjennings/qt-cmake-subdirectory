// foo.h
#ifndef MY_FOO
#define MY_FOO

#include <string>

namespace myfoo
{
  
  class Foo
  {
  public:
    enum Bar { A, B, C };
    explicit Foo();
    std::string nextText();
  private:
    Bar bar;
  };

}
#endif // MY_FOO

