// foo.cpp
#include "../include/foo.h"
#include <filesystem>

namespace fs = std::filesystem;

namespace myfoo {
  Foo::Foo()
    : bar{Foo::Bar::A}
  {}
  
  std::string Foo::nextText()
  {
	fs::path p(".");
	bool x = fs::exists(p);
	std::string s;
	
	switch (bar) {
      case Foo::Bar::A:
        bar = Foo::Bar::B;
        s.append("Set...");
        break;
      case Foo::Bar::B:
        bar = Foo::Bar::C;
        s.append("GO!!!");
        break;
      default:
        bar = Foo::Bar::A;
        s.append("Ready...");
    }

	//	s.append(x ? "yes" : "no");
	s.append("\n").append(fs::absolute(p));
	return s;
  }
}
