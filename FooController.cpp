// FooController.cpp
#include <QDebug>
#include "FooController.h"

FooController::FooController(QObject* parent)
  : QObject(parent)
  , theText{"Ready..."}
  , foo{}
{}

QString FooController::getTheText() const
{
  return theText;
}

void FooController::changeTheText()
{
  theText = QString().fromStdString(foo.nextText());
  emit theTextChanged();
  qDebug() << "theText: " << theText;
}
