// FooController.h
#ifndef MY_FOO_CONTROLLER
#define MY_FOO_CONTROLLER

#include <QObject>
#include <QString>
#include "foo.h"

class FooController : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString theText READ getTheText NOTIFY theTextChanged)

 public:
  explicit FooController(QObject* parent = nullptr);

  QString getTheText() const;

 signals:
  void theTextChanged();

 public slots:
  void changeTheText();

 private:
  QString theText;
  myfoo::Foo foo;
};

#endif // MY_FOO_CONTROLLER
